import { LOCALES } from "../locales";
const data = {
	[LOCALES.ENGLISH]: {
		navHome: "Home",
		navAbout: "About",
		navPricing: "Pricing",
		heroHeading: "Great software is built with amazing developers",
		heroDesc:
			"We help build and manage a team of world-class developers to bring your vision to life",
		heroInput: "Subscribe newsletter",
		heroBtn: "Subscribe",
		aboutHead: "Free Customer Support to ensure what you like to expect",
		aboutDesc1:
			"We offer a risk-free trial period of up to two weeks. You will only have to pay if you are happy with the developer and wish to continue. If you are unsatisfied, we’ll refund payment or fix issues on our dime period customers.",
		aboutDesc2:
			"If you are happy with the developer and wish to continue. If you are unsatisfied, we’ll refund payment or fix issues.",
		pricingHead1: "Free Plan",
		pricingHead2: "Business king",
		pricingHead3: "Pro Master",
		pricingLabel1: "For Small teams or office",
		pricingLabel2: "For Enterprise business",
		pricingLabel3: "For pro level developers",
		priceingService1: "Drag & Drop Builder",
		priceingService2: "1,000's of Templates",
		priceingService3: "Blog Support Tools",
		priceingService4: "eCommerce Store",
		priceingCurr: "$",
		pricingMonthly: "Monthly",
		pricingBtn1: "Start free trial",
		pricingBtn2: "Create Account",
	},
};
export default data;
