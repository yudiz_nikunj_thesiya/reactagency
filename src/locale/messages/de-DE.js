import { LOCALES } from "../locales";

const data = {
	[LOCALES.GERMAN]: {
		navHome: "Zuhause",
		navAbout: "Über",
		navPricing: "Preisgestaltung",
		heroHeading:
			"Großartige Software wird von großartigen Entwicklern entwickelt",
		heroDesc:
			"Wir helfen beim Aufbau und Management eines Teams von Weltklasse-Entwicklern, um Ihre Vision zum Leben zu erwecken",
		heroInput: "Newsletter abonnieren",
		heroBtn: "Abonnieren",
		aboutHead:
			"Kostenloser Kundensupport, um sicherzustellen, was Sie erwarten",
		aboutDesc1:
			"Wir bieten eine risikofreie Probezeit von bis zu zwei Wochen an. Sie müssen nur bezahlen, wenn Sie mit dem Entwickler zufrieden sind und fortfahren möchten. Wenn Sie unzufrieden sind, erstatten wir die Zahlung oder beheben Probleme bei unseren Dime-Period-Kunden.",
		aboutDesc2:
			"Wenn Sie mit dem Entwickler zufrieden sind und fortfahren möchten. Wenn Sie nicht zufrieden sind, erstatten wir die Zahlung oder beheben Probleme.",
		pricingHead1: "Kostenloser Plan",
		pricingHead2: "Business-König",
		pricingHead3: "Profi-Meister",
		pricingLabel1: "Für kleine Teams oder Büro",
		pricingLabel2: "Für Enterprise-Geschäfte",
		pricingLabel3: "Für professionelle Entwickler",
		priceingService1: "Drag & Drop Builder",
		priceingService2: "Tausende von Vorlagen",
		priceingService3: "Blog-Support-Tools",
		priceingService4: "E-Commerce-Shop",
		pricingMonthly: "Monatlich",
		priceingCurr: "€",
		pricingBtn1: "Kostenlos testen",
		pricingBtn2: "Benutzerkonto erstellen",
	},
};
export default data;
