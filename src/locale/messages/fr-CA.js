import { LOCALES } from "../locales";
const data = {
	[LOCALES.FRENCH]: {
		navHome: "Maison",
		navAbout: "Sur",
		navPricing: "Tarification",
		heroHeading:
			"Un excellent logiciel est construit avec des développeurs incroyables",
		heroDesc:
			"Nous aidons à construire et à gérer une équipe de développeurs de classe mondiale pour donner vie à votre vision",
		heroInput: "Abonnez-vous à la newsletter",
		heroBtn: "S'abonner",
		aboutHead:
			"Support client gratuit pour garantir ce que vous aimez attendre",
		aboutDesc1:
			"Nous offrons une période d'essai sans risque pouvant aller jusqu'à deux semaines. Vous n'aurez à payer que si vous êtes satisfait du développeur et souhaitez continuer. Si vous n'êtes pas satisfait, nous rembourserons le paiement ou résoudrons les problèmes de nos clients pendant la période de dix sous.",
		aboutDesc2:
			"Si vous êtes satisfait du développeur et souhaitez continuer. Si vous n'êtes pas satisfait, nous rembourserons le paiement ou réglerons les problèmes.",
		pricingHead1: "Forfait gratuit",
		pricingHead2: "Roi des affaires",
		pricingHead3: "Maître professionnel",
		pricingLabel1: "Pour les petites équipes ou le bureau",
		pricingLabel2: "Pour les entreprises",
		pricingLabel3: "Pour les développeurs de niveau professionnel",
		priceingService1: "Générateur de glisser-déposer",
		priceingService2: "Des milliers de modèles",
		priceingService3: "Outils d'assistance aux blogs",
		priceingService4: "Boutique de commerce électronique",
		pricingMonthly: "Mensuel",
		priceingCurr: "€",
		pricingBtn1: "Commencer l'essai gratuit",
		pricingBtn2: "Créer un compte",
	},
};
export default data;
