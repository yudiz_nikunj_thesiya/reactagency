import React from "react";
import Img from "../assets/img.png";
import Translate from "../locale/Translate";
import "../styles/herosection.scss";

const Herosection = () => {
	return (
		<div className="herosec">
			<div className="herosec-left">
				<span className="herosec-left--heading">
					{Translate("heroHeading")}
				</span>
				<span className="herosec-left--desc">{Translate("heroDesc")}</span>
				<div className="herosec-left--input">
					<input type="text" className="" placeholder="Subscribe newsletter" />
					<button>{Translate("heroBtn")}</button>
				</div>
			</div>

			<img src={Img} alt="illustration" />
		</div>
	);
};

export default Herosection;
