import React, { useState } from "react";
import "../styles/nav.scss";
import { RiMenu3Line } from "react-icons/ri";
import "../styles/verticalnav.scss";
import { MdOutlineClose } from "react-icons/md";
import { Link, NavLink } from "react-router-dom";
import { useStateValue } from "../state/StateProvider";
import Translate from "../locale/Translate";

const Nav = () => {
	const [{ lang }, dispatch] = useStateValue();

	const handleChange = (e) => {
		dispatch({
			type: "SET_LANG",
			lang: e.target.value,
		});
	};

	const [mobileNav, setMobileNav] = useState(false);
	return (
		<div className="container">
			<div className="nav">
				<Link
					to="/"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					<span className="logo">ReactAgency</span>
				</Link>
				<div className="nav__menu">
					<NavLink
						to="/"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						{Translate("navHome")}
					</NavLink>
					<NavLink
						to="/about"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						{Translate("navAbout")}
					</NavLink>
					<NavLink
						to="/pricing"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						{Translate("navPricing")}
					</NavLink>
				</div>
				<div className="nav__btns">
					<div className="select">
						<select onChange={handleChange}>
							<option value="en-us">English</option>
							<option value="de-de">German</option>
							<option value="fr-ca">French</option>
						</select>
					</div>
					<button className="nav__btns--btn-2">Register</button>
					<button
						className="nav__btns--btn-1"
						onClick={() => setMobileNav(true)}
					>
						<RiMenu3Line />
					</button>
				</div>
			</div>
			{mobileNav && (
				<div className="verticalnav">
					<div className="verticalnav__header">
						<span className="verticalnav__header--logo">ReactAgency</span>
						<button
							className="verticalnav__header--btn"
							onClick={() => setMobileNav(false)}
						>
							<MdOutlineClose />
						</button>
					</div>
					<div className="verticalnav__menu">
						<span>
							<NavLink
								to="/"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								{Translate("navHome")}
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/about"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								{Translate("navAbout")}
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/pricing"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								{Translate("navPricing")}
							</NavLink>
						</span>
					</div>
				</div>
			)}
		</div>
	);
};

export default Nav;
