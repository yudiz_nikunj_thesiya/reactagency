import React from "react";
import PricingCard from "./PricingCard";
import "../styles/pricingsection.scss";
import Translate from "../locale/Translate";


const PricingSection = () => {
	return (
		<div className="pricingsection">
			<PricingCard
				title={Translate("pricingHead1")}
				label={Translate("pricingLabel1")}
				service1={Translate("priceingService1")}
				service2={Translate("priceingService2")}
				service3={Translate("priceingService3")}
				rs={0}
				btn={Translate("pricingBtn1")}
			/>
			<PricingCard
				title={Translate("pricingHead2")}
				label={Translate("pricingLabel2")}
				service1={Translate("priceingService1")}
				service2={Translate("priceingService2")}
				service3={Translate("priceingService4")}
				rs={15}
				btn={Translate("pricingBtn2")}
			/>
			<PricingCard
				title={Translate("pricingHead3")}
				label={Translate("pricingLabel3")}
				service1={Translate("priceingService1")}
				service2={Translate("priceingService2")}
				service3={Translate("priceingService4")}
				rs={24}
				btn={Translate("pricingBtn2")}
			/>
		</div>
	);
};

export default PricingSection;
