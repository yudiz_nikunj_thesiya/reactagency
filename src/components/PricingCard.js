import React from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import Translate from "../locale/Translate";
import { FormattedMessage, FormattedNumber } from "react-intl";
import "../styles/pricingcard.scss";
import { useStateValue } from "../state/StateProvider";

const PricingCard = ({
	title,
	label,
	service1,
	service2,
	service3,
	rs,
	btn,
}) => {
	const [{ lang }, dispatch] = useStateValue();
	return (
		<div className="pricingcard">
			<div className="pricingcard-head">
				<span className="title">{title}</span>
				<span className="label">{label}</span>
			</div>
			<div className="pricingcard-services">
				<div className="service">
					<AiFillCheckCircle />
					<span>{service1}</span>
				</div>
				<div className="service">
					<AiFillCheckCircle />
					<span>{service2}</span>
				</div>
				<div className="service">
					<AiFillCheckCircle />
					<span>{service3}</span>
				</div>
			</div>
			<div className="pricingcard-end">
				<div className="pricingcard-end-price">
					<span className="price">
						<FormattedMessage id="priceingCurr">
							{(data) => <FormattedNumber value={data} currency={lang} />}
						</FormattedMessage>
					</span>
					<span className="price">{lang === "en-us" && rs}</span>
					<span className="price">{lang === "de-de" && rs * 0.91}</span>
					<span className="price">{lang === "fr-ca" && rs * 0.91}</span>
					<span className="monthly">/{Translate("pricingMonthly")}</span>
				</div>
				<button>{btn}</button>
			</div>
		</div>
	);
};

export default PricingCard;
