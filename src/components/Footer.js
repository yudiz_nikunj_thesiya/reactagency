import React from "react";
import "../styles/footer.scss";

const Footer = () => {
	return (
		<div className="footer">
			<span>All rights reserved. Design by </span>
			<a href="https://www.nikunjthesiya.tech/">Nikunj Thesiya</a>
		</div>
	);
};

export default Footer;
