import React from "react";
import Img from "../assets/about.png";
import Translate from "../locale/Translate";
import "../styles/banner.scss";

const Banner = () => {
	return (
		<div className="banner">
			<div className="banner-left">
				<span className="banner-left--heading">{Translate("heroDesc")}</span>
				<span className="banner-left--desc">{Translate("aboutDesc1")}</span>
				<span className="banner-left--desc">{Translate("aboutDesc2")}</span>
			</div>

			<img src={Img} alt="illustration" />
		</div>
	);
};

export default Banner;
