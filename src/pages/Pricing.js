import React from "react";
import Footer from "../components/Footer";
import Nav from "../components/Nav";
import PricingSection from "../components/PricingSection";

const Pricing = () => {
	return (
		<div>
			<Nav />
			<PricingSection />
			<Footer />
		</div>
	);
};

export default Pricing;
