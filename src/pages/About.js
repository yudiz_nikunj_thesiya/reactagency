import React from "react";
import Banner from "../components/Banner";
import Footer from "../components/Footer";
import Nav from "../components/Nav";

const About = () => {
	return (
		<div>
			<Nav />
			<Banner />
			<Footer/>
		</div>
	);
};

export default About;
