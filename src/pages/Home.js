import React from "react";
import Banner from "../components/Banner";
import Footer from "../components/Footer";
import Herosection from "../components/Herosection";
import Nav from "../components/Nav";
import PricingSection from "../components/PricingSection";

const Home = () => {
	return (
		<div>
			<Nav />
			<Herosection />
			<Banner />
			<PricingSection />
			<Footer />
		</div>
	);
};

export default Home;
