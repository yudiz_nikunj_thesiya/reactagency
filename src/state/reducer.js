export const initialState = {
	lang: "en-us",
};

function reducer(state, action) {
	switch (action.type) {
		case "SET_LANG":
			return {
				lang: action.lang,
			};

		default:
			return state;
	}
}

export default reducer;
