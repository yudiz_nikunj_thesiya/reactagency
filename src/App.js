import Home from "./pages/Home";
import { Routes, Route } from "react-router-dom";
import About from "./pages/About";
import Pricing from "./pages/Pricing";
import { useStateValue } from "./state/StateProvider";
import { I18nProvider } from "./locale";

function App() {
	const [{ lang }, dispatch] = useStateValue();
	return (
		<div className="App">
			<I18nProvider locale={lang}>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/about" element={<About />} />
					<Route path="/pricing" element={<Pricing />} />
				</Routes>
			</I18nProvider>
		</div>
	);
}

export default App;
