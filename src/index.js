import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { StateProvider } from "./state/StateProvider";
import reducer, { initialState } from "./state/reducer";

ReactDOM.render(
	<StateProvider initialState={initialState} reducer={reducer}>
		<BrowserRouter>
			<React.StrictMode>
				<App />
			</React.StrictMode>
		</BrowserRouter>
	</StateProvider>,
	document.getElementById("root")
);
